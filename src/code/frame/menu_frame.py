# -*- coding: utf-8 -*-
# @Author: Ultraxime
# @Date:   2023-01-26 16:59:24
# @Last Modified by:   Ultraxime
# @Last Modified time: 2023-01-27 20:40:09

# This file is part of Baston Tables Reader.

# Baston Tables Reader is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
# or any later version.

# Baston Tables Reader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Baston Tables Reader. If not, see <https://www.gnu.org/licenses/>.

"""
Module describing the menu frame
"""

import tkinter as tk
from tkinter import Button, LabelFrame
from typing import Callable, Tuple

from .default_frames import CustomFrame

from ..windows import Windows


class MenuFrame(CustomFrame, LabelFrame):
    """
    This class describes a menu frame.
    """
    def __init__(self,
                 parent,
                 choice: Callable[[Windows], None],
                 padx: Tuple[int, int] = (0, 5),
                 pady: Tuple[int, int] = (0, 5),
                 **kwargs):

        # Initialize the name
        CustomFrame.__init__(self, parent, **kwargs)
        LabelFrame.__init__(self, parent, text="Menu",
                            borderwidth=2, relief=tk.GROOVE, **kwargs)

        self.pack(side=tk.TOP, padx=padx, pady=pady)

        power_button = Button(
            self, width=22, text="Petits numéros",
            command=lambda: choice(Windows.POWER))
        power_button.grid(
            row=0, column=0, padx=0, pady=0)

        magic_button = Button(
            self, width=22, text="Magies",
            command=lambda: choice(Windows.MAGIC))
        magic_button.grid(
            row=2, column=0, padx=0, pady=0)
