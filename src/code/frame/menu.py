# -*- coding: utf-8 -*-
# @Author: Ultraxime
# @Date:   2023-01-20 07:37:15
# @Last Modified by:   Ultraxime
# @Last Modified time: 2023-01-27 20:39:10

# This file is part of Baston Tables Reader.

# Baston Tables Reader is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
# or any later version.

# Baston Tables Reader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Baston Tables Reader. If not, see <https://www.gnu.org/licenses/>.

"""
Module describing the menu class
"""

import tkinter as tk
from tkinter import Label, OptionMenu, StringVar
from typing import Callable, List

from .default_frames import CustomFrame


class Menu(CustomFrame):
    """
    This class describes a menu.
    """
    def __init__(self, parent: 'CustomFrame',
                 name: str,
                 var: StringVar,
                 command: Callable,
                 options: List[str],
                 help_msg: str,
                 **kwargs):
        super().__init__(parent, **kwargs)

        Label(self, text=name + " :").grid(
            row=0, column=0, sticky=tk.E, padx=5)

        menu = OptionMenu(
            self, var, options[0],
            *options[1:],
            command=command)
        menu.config(width=15)
        menu.grid(row=0, column=1, sticky=tk.W,
                  columnspan=1, padx=(0, 5), pady=(0, 5))
        self.add_tip(menu, help_msg)
