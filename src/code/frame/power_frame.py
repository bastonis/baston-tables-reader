# -*- coding: utf-8 -*-
# @Author: Ultraxime
# @Date:   2023-01-26 17:21:18
# @Last Modified by:   Ultraxime
# @Last Modified time: 2023-02-02 13:43:28

# This file is part of Baston Tables Reader.

# Baston Tables Reader is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
# or any later version.

# Baston Tables Reader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Baston Tables Reader. If not, see <https://www.gnu.org/licenses/>.

"""
Module describing the power frame
"""

import tkinter as tk
from tkinter import Button, Entry, Label, LabelFrame, Event, Canvas
from typing import Tuple, Optional

from PIL import Image
from PIL.ImageTk import PhotoImage

from ...tables.src.latex import create_preview, get_power

from .default_frames import CustomFrame


class PowerFrame(CustomFrame, LabelFrame):
    """
    This class describes a power frame.
    """
    _power: Entry
    _canvas: Optional[Canvas]
    _photo: Optional[PhotoImage]

    def __init__(self,
                 parent,
                 padx: Tuple[int, int] = (0, 5),
                 pady: Tuple[int, int] = (0, 5),
                 **kwargs):

        # Initialize the name
        CustomFrame.__init__(self, parent, **kwargs)
        LabelFrame.__init__(self, parent, text="Petits Numéros",
                            borderwidth=2, relief=tk.GROOVE, **kwargs)

        self.grid(padx=padx, pady=pady)

        Label(self, text="Petit numéro : ").grid(
            row=0, column=0, sticky=tk.E, padx=5)

        self._power = Entry(self, width=10)
        self._power.grid(row=0, column=1, sticky=tk.W,
                         padx=5, pady=5)
        self._power.bind("<Return>", self._update_power)
        self._power.bind("<KP_Enter>", self._update_power)

        button = Button(self, text="Valider", command=self._update_power)
        button.grid(row=1, columnspan=2)

        self._canvas = None
        self._phot = None

    def _update_power(self, _: Optional[Event] = None):
        try:
            create_preview(get_power(self._power.get()), True)
            if self._canvas is not None:
                self._canvas.destroy()
            image = Image.open("tmp.png")
            ratio = round(image.size[0] / 600)
            ratio = max(ratio, 1)
            image = image.reduce(ratio)
            if image.mode == "P":
                image = image.convert("RGBA")
            if image.mode == "RGB":
                a_channel = Image.new('L', image.size, 255)
                image.putalpha(a_channel)
            self._photo = PhotoImage(image)
            self._canvas = Canvas(self,
                                  width=image.size[0],
                                  height=image.size[1],
                                  borderwidth=0,
                                  relief=tk.RIDGE)
            self._canvas.grid(row=0, rowspan=3, column=2)
            #self._canvas.configure(background="white")
            self._canvas.create_image(0, 0, anchor=tk.NW, image=self._photo)
        except KeyError:
            pass
