# -*- coding: utf-8 -*-
# @Author: Ultraxime
# @Date:   2023-01-26 17:20:08
# @Last Modified by:   Ultraxime
# @Last Modified time: 2023-01-27 20:42:58

# This file is part of Baston Tables Reader.

# Baston Tables Reader is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
# or any later version.

# Baston Tables Reader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Baston Tables Reader. If not, see <https://www.gnu.org/licenses/>.

"""
Module describing the magic frame
"""

from .default_frames import CustomFrame


class MagicFrame(CustomFrame):
    """
    This class describes a magic frame.
    """
