# -*- coding: utf-8 -*-
# @Author: Ultraxime
# @Date:   2022-11-17 14:42:19
# @Last Modified by:   Ultraxime
# @Last Modified time: 2023-01-27 20:38:15

# This file is part of Baston Tables Reader.

# Baston Tables Reader is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
# or any later version.

# Baston Tables Reader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Baston Tables Reader. If not, see <https://www.gnu.org/licenses/>.

"""
Module describying the frame used for the interface
"""

from tkinter import Frame, Widget

from ..multithreaded import MultithreadedObject


class CustomFrame(MultithreadedObject, Frame):
    """
    This class describes a custom frame.
    """
    _parent: 'CustomFrame'

    def __init__(self, parent: 'CustomFrame', **kwargs):
        self._parent = parent
        new_kwargs = {k: v for k, v in kwargs.items()
                      if k not in ["text","borderwidth", "relief"]}
        super().__init__(parent, **new_kwargs)

    def add_tip(self, widget: Widget, tip: str):
        """
        Adds a tip.

        :param      widget:  The widget
        :type       widget:  Widget
        :param      tip:     The tip
        :type       tip:     str
        """
        self._parent.add_tip(widget, tip)
