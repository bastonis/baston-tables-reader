# -*- coding: utf-8 -*-
# @Author: Ultraxime
# @Date:   2023-01-02 20:37:00
# @Last Modified by:   Ultraxime
# @Last Modified time: 2023-01-27 20:37:04

# This file is part of Baston Tables Reader.

# Baston Tables Reader is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
# or any later version.

# Baston Tables Reader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Baston Tables Reader. If not, see <https://www.gnu.org/licenses/>.

"""
Module for all the frame used by the software
"""

__all__ = ["CustomFrame",
		   "MenuFrame",
		   "PowerFrame",
		   "MagicFrame"]

from .default_frames import CustomFrame

from .menu_frame import MenuFrame
from .power_frame import PowerFrame
from .magic_frame import MagicFrame
