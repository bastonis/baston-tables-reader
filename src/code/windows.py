# -*- coding: utf-8 -*-
# @Author: Ultraxime
# @Date:   2023-01-26 16:49:27
# @Last Modified by:   Ultraxime
# @Last Modified time: 2023-01-27 20:43:38

# This file is part of Baston Tables Reader.

# Baston Tables Reader is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
# or any later version.

# Baston Tables Reader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Baston Tables Reader. If not, see <https://www.gnu.org/licenses/>.

"""
Module describing the Windows enum
"""

from enum import Enum, auto
from typing import List


class Windows(Enum):
    """
    This class describes windows.
    """
    POWER = auto()
    MAGIC = auto()

    @classmethod
    def available(cls) -> List[str]:
        """
        List the available member of the enum

        :param      cls:  The cls
        :type       cls:  type

        :returns:   The list of available member
        :rtype:     List
        """
        return [member.get_name() for member in cls]

    @classmethod
    def _missing_(cls, value: str):
        value = value.upper().replace(" ", "_")
        for member in cls:
            if member.name == value:
                return member
        return None

    def get_name(self) -> str:
        """
        Gets the name.

        :returns:   The name.
        :rtype:     str
        """
        return self.name.lower().capitalize().replace("_", " ")

    def copy(self):
        """
        Copies the object.

        :returns:   A copy of the object
        :rtype:     The objet class
        """
        return type(self)(self.get_name())
