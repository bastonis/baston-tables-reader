# -*- coding: utf-8 -*-
# @Author: Ultraxime
# @Date:   2022-11-09 17:16:18
# @Last Modified by:   Ultraxime
# @Last Modified time: 2023-01-27 20:41:23

# This file is part of Baston Tables Reader.

# Baston Tables Reader is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
# or any later version.

# Baston Tables Reader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Baston Tables Reader. If not, see <https://www.gnu.org/licenses/>.

"""
Module describing the interface class
"""

from .frame import (CustomFrame, MenuFrame, PowerFrame, MagicFrame)

from .windows import Windows


class Interface(CustomFrame):
    """
    This class describes an interface.
    """
    _menu_bar: MenuFrame

    def __init__(self, fenetre, **kwargs):
        super().__init__(fenetre, **kwargs)

        self.pack()

        self._menu_bar = MenuFrame(self, self._menu_choice)
        self._menu_bar.grid(row=0, column=0)

        self._content = PowerFrame(self)
        self._content.grid(row=0, column=1)

    def _menu_choice(self, window: Windows):
        match window:
            case Windows.POWER:
                if isinstance(self._content, PowerFrame):
                    return
                self._content.destroy()
                self._content = PowerFrame(self)
                self._content.grid(row=0, column=1)
            case Windows.MAGIC:
                if isinstance(self._content, MagicFrame):
                    return
                self._content.destroy()
                self._content = MagicFrame(self)
                self._content.grid(row=0, column=1)
            case _:
                raise ValueError("Unkown Window: " + window.get_name())
