# -*- coding: utf-8 -*-
# @Author: brunomaxime
# @Date:   2023-01-26 16:28:57
# @Last Modified by:   Ultraxime
# @Last Modified time: 2023-01-27 20:38:32

# This file is part of Baston Tables Reader.

# Baston Tables Reader is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
# or any later version.

# Baston Tables Reader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Baston Tables Reader. If not, see <https://www.gnu.org/licenses/>.

"""
Main Module of Baston Tables Reader
"""

from tkinter import Tk, PhotoImage

from src.code.interface import Interface


if __name__ == "__main__":
    fenetre = Tk()
    fenetre.withdraw()  # hide the window
    fenetre.after(0, fenetre.deiconify)  # show it again when done
    fenetre.resizable(False, False)
    fenetre.title("Tables")
    fenetre.iconphoto(False, PhotoImage(file="src/pic/shield.png"))
    interface = Interface(fenetre)
    interface.mainloop()
